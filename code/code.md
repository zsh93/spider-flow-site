# 项目介绍

## 文件结构

### common后端结构

```text
├── spider-flow-common  // 定义了一些基础的类和接口
│       └── annotation            // 自定义注解，用于描述方法中的参数和返回值
│       └── common         		  // 存放controller基类
│       └── concurrent   		  // 存放线程和提交策略相关代码
│       └── context			      // 存放上下文相关类
│       └── enums			      // 存放流程通知相关代码
│       └── executor     		  // 存放执行相关接口
│       		└── ExpressionEngine                  // 表达式引擎接口
│       		└── FunctionExecutor                  // 自定义函数执行器接口
│       		└── FunctionExtension                 // 扩展函数执行器接口
│       		└── PluginConfig                      // 插件配置器接口
│       		└── ShapeExecutor                     // 任务执行器接口
│       		└── Grammerable                       // 可语法化接口
│       └── expression      	  // 表达式相关接口
│       └── io 					  // 文件相关操作工具类
│       └── listener      	      // 任务监听器接口
│       └── model      	       	  // 任务执行需要使用的部分实体类定义
│       └── respose      	      // 返回接口
│       └── utils      	          // 任务监听器接口
├── spider-flow-core  // 任务执行核心模块
│       └── exception             // 自定义异常
│       └── executor         	  // 存放执行相关实现
│       		└── extension              // 扩展函数执行器实现类
│       		└── function               // 自定义函数执行器实现类
│       		└── shape                  // 任务执行器相关实现类
│       └── executor         	  // 存放controller基类
│       └── executor         	  // 存放controller基类
│       └── executor         	  // 存放controller基类
│       └── executor         	  // 存放controller基类
│       └── executor         	  // 存放controller基类



```

